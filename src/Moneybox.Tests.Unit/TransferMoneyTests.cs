﻿using Moneybox.App;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moq;
using NUnit.Framework;
using System;

namespace Moneybox.Tests.Unit
{
    [TestFixture]
    public class TransferMoneyTests
    {
        private Mock<IAccountRepository> _accountRepository;
        private Mock<INotificationService> _notificationService;

        private Account FromAccount { get; set; }

        private Account ToAccount { get; set; }

        private TransferMoney _transferMoney;

        [SetUp]
        public void Setup()
        {
            _accountRepository = new Mock<IAccountRepository>();
            _notificationService = new Mock<INotificationService>();

            _transferMoney = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            FromAccount = new Account()
            {
                Id = Guid.NewGuid(),
                Balance = 1000m,
                User = new User { Email = "from@fakeEmail.com" },
                Withdrawable = 800m
            };

            ToAccount = new Account()
            {
                Id = Guid.NewGuid(),
                Balance = 0,
                User = new User { Email = "to@fakeEmail.com" },
                PaidIn = 0
            };

            _accountRepository.Setup(x => x.GetAccountById(FromAccount.Id)).Returns(FromAccount);
            _accountRepository.Setup(x => x.GetAccountById(ToAccount.Id)).Returns(ToAccount);
        }

        [Test]
        public void When_FromAccountIdIsNotValid_ThrowInvalidOperationException()
        {
            // given
            _accountRepository.Setup(x => x.GetAccountById(FromAccount.Id)).Returns((Account)null);

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _transferMoney.Execute(FromAccount.Id, ToAccount.Id, 10);
            }, "Account From Id is not valid");
        }

        [Test]
        public void When_TroAccountIdIsNotValid_ThrowInvalidOperationException()
        {
            // given
            _accountRepository.Setup(x => x.GetAccountById(ToAccount.Id)).Returns((Account)null);

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _transferMoney.Execute(FromAccount.Id, ToAccount.Id, 10);
            }, "Account To Id is not valid");
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(-100)]
        public void When_AmountIsInvalid_ThrowException(decimal amount)
        {
            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _transferMoney.Execute(FromAccount.Id, ToAccount.Id, amount);
            }, "Amount is not valid");
        }

        [TestCase(5, 4)]
        public void When_InsufficientFunds_ThrowInvalidOperationException(decimal amount, decimal fromBalance)
        {
            // given
            FromAccount.Balance = fromBalance;

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _transferMoney.Execute(FromAccount.Id, ToAccount.Id, amount);
            }, "Insufficient funds");
        }

        [TestCase(5, 3996)]
        public void When_PayInLimitIsReached_ThrowInvalidOperationException(decimal amount, decimal paidIn)
        {
            // given
            ToAccount.PaidIn = paidIn;

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _transferMoney.Execute(FromAccount.Id, ToAccount.Id, amount);
            }, "Account pay-in limit reached");
        }

        [TestCase(1000, 1200)]
        [TestCase(400, 400)]
        public void When_FundsLow_NotifyFromUser(decimal amount, decimal fromBalance)
        {
            // given
            FromAccount.Balance = fromBalance;

            _notificationService.Setup(x => x.NotifyFundsLow(It.IsAny<string>())).Verifiable();

            // when
            _transferMoney.Execute(FromAccount.Id, ToAccount.Id, amount);

            // then
            _notificationService.Verify(x => x.NotifyFundsLow(FromAccount.User.Email), Times.Once);
        }

        [TestCase(400)]
        [TestCase(1)]
        public void When_AllGood_ShouldUpdateBothAccounts(decimal amount)
        {
            // given
            var fromBalance = FromAccount.Balance;
            var fromWithdrawable = FromAccount.Withdrawable;
            var toBalance = ToAccount.Balance;
            var toPaidIn = ToAccount.PaidIn;

            _accountRepository.Setup(x => x.Update(It.IsAny<Account>())).Verifiable();

            // when
            _transferMoney.Execute(FromAccount.Id, ToAccount.Id, amount);

            // then
            _accountRepository.Verify(x => x.Update(It.Is<Account>(a =>
                a.Id == FromAccount.Id &&
                a.Balance == fromBalance - amount &&
                a.Withdrawable == fromWithdrawable - amount)), Times.Once);

            _accountRepository.Verify(x => x.Update(It.Is<Account>(a =>
                a.Id == ToAccount.Id &&
                a.Balance == toBalance + amount &&
                a.PaidIn == toPaidIn + amount)), Times.Once);
        }
    }
}

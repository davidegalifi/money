﻿using Moneybox.App;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moq;
using NUnit.Framework;
using System;

namespace Moneybox.Tests.Unit
{
    [TestFixture]
    public class WithdrawMoneyTests
    {
        private Mock<IAccountRepository> _accountRepository;
        private Mock<INotificationService> _notificationService;

        private WithdrawMoney _withdrawMoney;

        private Account Account { get; set; }

        [SetUp]
        public void Setup()
        {
            _accountRepository = new Mock<IAccountRepository>();
            _notificationService = new Mock<INotificationService>();

            _withdrawMoney = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            Account = new Account()
            {
                Id = Guid.NewGuid(),
                Balance = 1000m,
                User = new User { Email = "from@fakeEmail.com" },
                Withdrawable = 800m
            };

            _accountRepository.Setup(x => x.GetAccountById(Account.Id)).Returns(Account);
        }

        [Test]
        public void When_AccountIdIsNotValid_ThrowInvalidOperationException()
        {
            // given
            _accountRepository.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Returns((Account)null);

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _withdrawMoney.Execute(Guid.NewGuid(), 10);
            }, "Account Id is not valid");
        }

        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(-100)]
        public void When_AmountIsInvalid_ThrowException(decimal amount)
        {
            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _withdrawMoney.Execute(Account.Id, amount);
            }, "Amount is not valid");
        }

        [Test]
        public void When_InsufficientFunds_ThrowInvalidOperationException()
        {
            // given
            decimal amount = 5;
            Account.Balance = amount - 1;

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _withdrawMoney.Execute(Guid.NewGuid(), amount);
            }, "Insufficient funds to make withdrawal");
        }

        [TestCase(500, 1)]
        [TestCase(401, 100)]
        [TestCase(400, 399)]
        public void When_WithdrawalLimitReached_ThrowInvalidOperationException(decimal amount, decimal withdrable)
        {
            // given
            Account.Balance = amount + 1;
            Account.Withdrawable = withdrable;

            // when/then
            Assert.Throws<InvalidOperationException>(() =>
            {
                _withdrawMoney.Execute(Account.Id, amount);
            }, "Withdrawal limit reached");
        }

        [TestCase(1000, 1200)]
        [TestCase(400, 400)]
        public void When_FundsLow_NotifyUser(decimal amount, decimal balance)
        {
            // given
            Account.Balance = balance;
            Account.Withdrawable = amount + 1;

            _notificationService.Setup(x => x.NotifyFundsLow(It.IsAny<string>())).Verifiable();

            // when
            _withdrawMoney.Execute(Account.Id, amount);

            // then
            _notificationService.Verify(x => x.NotifyFundsLow(Account.User.Email), Times.Once);
        }

        [TestCase(400)]
        [TestCase(1)]
        public void When_AllGood_ShouldUpdateAccount(decimal amount)
        {
            // given
            var balance = Account.Balance;
            var withdrawable = Account.Withdrawable;

            _accountRepository.Setup(x => x.Update(It.IsAny<Account>())).Verifiable();

            // when
            _withdrawMoney.Execute(Account.Id, amount);

            // then
            _accountRepository.Verify(x => x.Update(It.Is<Account>(a =>
            a.Balance == balance - amount &&
            a.Withdrawable == withdrawable - amount)));
        }
    }
}

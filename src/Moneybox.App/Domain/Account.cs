﻿using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Moneybox.Tests.Unit")]
namespace Moneybox.App
{
    public class Account
    {
        public const decimal PayInLimit = 4000m;
        public const decimal LowFundsLimit = 500m;

        public Guid Id { get; internal set; }

        public User User { get; set; }

        public decimal Balance { get; internal set; }

        public decimal Withdrawable { get; internal set; }

        public decimal PaidIn { get; internal set; }

        public void TryWithdraw(decimal amount, bool withdrawing = false)
        {
            if (amount <= 0)
                throw new InvalidOperationException("Amount is not valid");

            if (Balance - amount < 0m)
            {
                throw new InvalidOperationException("Insufficient funds");
            }

            if (withdrawing && Withdrawable - amount < 0m)
            {
                throw new InvalidOperationException("Withdrawal limit reached");
            }

            Balance = Balance - amount;
            Withdrawable = Withdrawable - amount;
        }

        public void TryPayIn(decimal amount)
        {
            if (amount <= 0)
                throw new InvalidOperationException("Amount is not valid");

            if (PaidIn + amount > PayInLimit)
            {
                throw new InvalidOperationException("Account pay-in limit reached");
            }

            Balance = Balance + amount;
            PaidIn = PaidIn + amount;
        }
    }
}

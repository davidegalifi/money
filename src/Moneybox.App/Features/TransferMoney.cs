﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App.Features
{
    public class TransferMoney
    {
        private IAccountRepository _accountRepository;
        private INotificationService _notificationService;

        public TransferMoney(IAccountRepository accountRepository, INotificationService notificationService)
        {
            _accountRepository = accountRepository;
            _notificationService = notificationService;
        }

        public void Execute(Guid fromAccountId, Guid toAccountId, decimal amount)
        {
            var from = _accountRepository.GetAccountById(fromAccountId);
            var to = _accountRepository.GetAccountById(toAccountId);

            if (from == null)
                throw new InvalidOperationException("Account From Id is not valid");

            if (to == null)
                throw new InvalidOperationException("Account To Id is not valid");

            from.TryWithdraw(amount);
            to.TryPayIn(amount);

            if (from.Balance < 500m)
            {
                _notificationService.NotifyFundsLow(from.User.Email);
            }

            if (Account.PayInLimit - to.PaidIn < 500m)
            {
                _notificationService.NotifyApproachingPayInLimit(to.User.Email);
            }

            _accountRepository.Update(from);
            _accountRepository.Update(to);
        }
    }
}
